package com.lbc.testtechniquelbc.di.retrofit.instance.room.dataBase

import androidx.room.Database
import androidx.room.RoomDatabase
import com.lbc.testtechniquelbc.domain.models.dataClass.LbcModel

@Database(
    version = 1,
    entities = [LbcModel::class]
)
abstract class RoomDb : RoomDatabase() {
    abstract fun dao(): DaoDataBase

    companion object {
        const val DATABASE_NAME: String = "room_db"
    }
}