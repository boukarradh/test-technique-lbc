package com.lbc.testtechniquelbc.di.retrofit.instance.retrofitInterceptor

import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.Response


class RetrofitInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        var request = chain.request()
        request = request.newBuilder()
            .method(request.method, request.body)
            .build()

        return chain.proceed(request)

        /**Handling Network Error IOException*/

    }
}