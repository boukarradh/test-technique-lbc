package com.lbc.testtechniquelbc.di

import android.content.Context
import androidx.room.Room
import com.lbc.testtechniquelbc.data.repository.MainRepository
import com.lbc.testtechniquelbc.di.retrofit.instance.retrofitInterceptor.RetrofitInterceptor
import com.lbc.testtechniquelbc.di.retrofit.instance.room.dataBase.DaoDataBase
import com.lbc.testtechniquelbc.di.retrofit.instance.room.dataBase.RoomDb
import com.lbc.testtechniquelbc.domain.models.objects.Url.URL
import com.lbc.testtechniquelbc.domain.network.MainServices
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object MainModule {

    @Provides
    fun provideBaseUrl(): String {
        return URL
    }

    @Provides
    @Singleton
    fun provideConverterFactory(): Converter.Factory {
        return GsonConverterFactory.create()
    }

    @Provides
    @Singleton
    fun provideInterceptor(): Interceptor {
        return RetrofitInterceptor()
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(interceptor: Interceptor): OkHttpClient {
        return OkHttpClient.Builder().addInterceptor(interceptor)
            .readTimeout(20, TimeUnit.SECONDS)
            .connectTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(20, TimeUnit.SECONDS)
            .callTimeout(20, TimeUnit.SECONDS)
            .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(
        baseURL: String,
        converter: Converter.Factory,
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseURL)
            .addConverterFactory(converter)
            .client(okHttpClient)
            .build()
    }

    @Singleton
    @Provides
    fun provideApiService(retrofit: Retrofit): MainServices {
        return retrofit.create(MainServices::class.java)
    }

    @Singleton
    @Provides
    fun provideDAO(db: RoomDb): DaoDataBase {
        return db.dao()
    }

    @Singleton
    @Provides
    fun provideRoom(@ApplicationContext context: Context): RoomDb {
        return Room
            .databaseBuilder(
                context,
                RoomDb::class.java,
                RoomDb.DATABASE_NAME
            )
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideRepository(dao :DaoDataBase,webService :MainServices): MainRepository {
        return MainRepository(dao,webService)
    }
}