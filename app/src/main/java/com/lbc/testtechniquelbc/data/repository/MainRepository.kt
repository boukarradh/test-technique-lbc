package com.lbc.testtechniquelbc.data.repository

import com.lbc.testtechniquelbc.di.retrofit.instance.room.dataBase.DaoDataBase
import com.lbc.testtechniquelbc.domain.models.`interface`.MainRepositoryImpl
import com.lbc.testtechniquelbc.domain.network.MainServices
import javax.inject.Inject

class MainRepository @Inject constructor(
    private val daoDb: DaoDataBase,
    private val webService: MainServices,
) : MainRepositoryImpl {
    override suspend fun getListOfItems() = safeCall {

    }
}