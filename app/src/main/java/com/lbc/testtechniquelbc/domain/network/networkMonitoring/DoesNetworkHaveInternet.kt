package com.lbc.testtechniquelbc.domain.network.networkMonitoring

import java.io.IOException
import java.net.InetSocketAddress
import javax.net.SocketFactory


/**
 * Send a ping to googles primary DNS.
 * If successful, that means we have internet.
 */
object DoesNetworkHaveInternet {

    fun execute(socketFactory: SocketFactory): Boolean {
        return try {
            val socket = socketFactory.createSocket() ?: throw IOException("Socket is null.")
            socket.connect(InetSocketAddress("8.8.4.4", 53), 3000)
            socket.close()
            true
        } catch (e: IOException) {
            false
        }
    }
}