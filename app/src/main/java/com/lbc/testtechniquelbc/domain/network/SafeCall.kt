package com.lbc.testtechniquelbc.domain.network

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import com.lbc.testtechniquelbc.domain.dataState.DataState
import okio.IOException
import retrofit2.HttpException
import java.net.SocketTimeoutException

interface SafeCall {
    suspend fun <T> safeCall(
        apiCall: suspend () -> T
    ): DataState<T> {
        return withContext(Dispatchers.IO) {
            try {
                DataState.Success(apiCall.invoke())
            } catch (throwable: Throwable) {
                when (throwable) {
                    is HttpException  -> {
                        when (throwable.code()) {
                            400 -> DataState.Failure(
                                DataState.ErrorType.BAD_REQUEST,
                                throwable.code(),
                                throwable.response()?.errorBody()
                            )
                            401 -> DataState.Failure(
                                DataState.ErrorType.UNAUTHORIZED,
                                throwable.code(),
                                throwable.response()?.errorBody()
                            )
                            403 -> DataState.Failure(
                                DataState.ErrorType.FORBIDDEN,
                                throwable.code(),
                                throwable.response()?.errorBody()
                            )
                            404 -> DataState.Failure(
                                DataState.ErrorType.NOT_FOUND,
                                throwable.code(),
                                throwable.response()?.errorBody()
                            )
                            408 -> DataState.Failure(
                                DataState.ErrorType.TIMEOUT,
                                throwable.code(),
                                throwable.response()?.errorBody()
                            )
                            500 -> DataState.Failure(
                                DataState.ErrorType.INTERNAL_SERVER_ERROR,
                                throwable.code(),
                                throwable.response()?.errorBody()
                            )
                            502 -> DataState.Failure(
                                DataState.ErrorType.BAD_GATEWAY,
                                throwable.code(),
                                throwable.response()?.errorBody()
                            )
                            503 -> DataState.Failure(
                                DataState.ErrorType.SERVICE_UNAVAILABLE,
                                throwable.code(),
                                throwable.response()?.errorBody()
                            )

                            else -> {
                                DataState.Failure(
                                    DataState.ErrorType.UNKNOWN,
                                    throwable.code(),
                                    throwable.response()?.errorBody()
                                )
                            }
                        }
                    }

                    is IOException -> {
                        DataState.Failure(
                            DataState.ErrorType.NETWORK,
                            null,
                            throwable.message
                        )
                    }
                    is SocketTimeoutException -> {
                        DataState.Failure(
                            DataState.ErrorType.TIMEOUT,
                            null,
                            throwable.message
                        )
                    }

                    else -> {
                        DataState.Failure(
                            DataState.ErrorType.UNKNOWN,
                            null, throwable.message
                        )
                    }
                }
            }
        }
    }

    }