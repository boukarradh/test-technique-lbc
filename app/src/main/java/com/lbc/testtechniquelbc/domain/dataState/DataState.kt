package com.lbc.testtechniquelbc.domain.dataState

sealed class DataState<out T> {
    class Success<out T>(val body: T) : DataState<T>()
    data class Failure(
        val errorType: ErrorType,
        val errorCode: Any?,
        val errorBody: Any?
    ) : DataState<Nothing>()

    enum class ErrorType {
        EMPTY_DATA,
        NETWORK,
        TIMEOUT,
        UNKNOWN,
        BAD_REQUEST,
        UNAUTHORIZED,
        FORBIDDEN,
        NOT_FOUND,
        INTERNAL_SERVER_ERROR,
        BAD_GATEWAY,
        SERVICE_UNAVAILABLE,
    }

    object Loading : DataState<Nothing>()


}
