package com.lbc.testtechniquelbc.domain.models.dataClass

import androidx.room.Entity
import androidx.room.PrimaryKey
@Entity(tableName = "lbc_table")

data class LbcModel(
    @PrimaryKey(autoGenerate = false)

    var albumId :String,
    var id :String,
    var title :String,
    var url:String,
    var thumbnailUrl :String
)
