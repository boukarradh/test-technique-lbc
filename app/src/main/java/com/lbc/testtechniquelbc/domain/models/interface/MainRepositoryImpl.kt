package com.lbc.testtechniquelbc.domain.models.`interface`

import com.lbc.testtechniquelbc.domain.dataState.DataState
import com.lbc.testtechniquelbc.domain.network.SafeCall

interface MainRepositoryImpl :SafeCall {
    suspend fun getListOfItems(): DataState<Unit>
}