package com.lbc.testtechniquelbc.presentation.mainFragment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lbc.testtechniquelbc.R
import com.lbc.testtechniquelbc.domain.models.dataClass.LbcModel
import kotlinx.android.synthetic.main.item_layout.view.*
import net.ubisolutions.cmp.presentation.selectLocationFragment.adapter.ItemClickListener


class ItemsListAdapter(private val context: Context) :
    RecyclerView.Adapter<ItemsListAdapter.ItemViewHolder>() {
    private lateinit var itemClickListener: ItemClickListener
    private var itemsList = emptyList<LbcModel>().toMutableList()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ItemViewHolder {

        return ItemViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_layout, parent, false),
            this.itemClickListener
        )
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.itemView.title.text = itemsList[position].title
        holder.itemView.album.text = itemsList[position].albumId
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }


    fun fillData(tag: List<LbcModel>) {
        this.itemsList.addAll(tag)
        notifyDataSetChanged()
    }


    fun setOnClickListener(listener: ItemClickListener) {
        this.itemClickListener = listener
    }

    inner class ItemViewHolder
        (private val item: View, scanTagClickListener: ItemClickListener) :
        RecyclerView.ViewHolder(item) {

        init {
            itemView.setOnClickListener {
                scanTagClickListener.onItemClick(bindingAdapterPosition)
            }

        }

    }
}
