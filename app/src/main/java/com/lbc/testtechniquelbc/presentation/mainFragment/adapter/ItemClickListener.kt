package net.ubisolutions.cmp.presentation.selectLocationFragment.adapter

interface ItemClickListener {
    fun onItemClick(position: Int)
}